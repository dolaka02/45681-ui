using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour  // Spacja -10 HP    Shift -10 ST 
{
    public float speed = 5f;

    public float currentStamina;
    public float maxStamina = 100f;


    public float currentHealth;
    public float maxHealth = 100f;

    public  float currentEXP;
    public float MaxEXP = 10f;


    public Health healthBar;
    public StaminaBar staminaBar;
    public EXPbar XPBar;

    private void Start()
    {

        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        currentStamina = maxStamina;
        staminaBar.SetMaxStamina(maxStamina);

        currentEXP = 0;
        XPBar.SetMinEXP(currentEXP);


    }



    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(horizontal, vertical);
        transform.Translate(movement * speed * Time.deltaTime);


        if (Input.GetKeyDown( KeyCode.Space ))
        {
            TakeDamage(10);
        } 

        if (Input.GetKeyDown( KeyCode.LeftShift ))
        {
            Sprint(10);
        }

        if (!IsPlayerMoving())
        {

            currentStamina += 0.002f;
            staminaBar.SetStamina(currentStamina);
            if (currentStamina > maxStamina)
            {
                currentStamina = maxStamina;
            }
        }
        else
        {
            currentEXP += 0.002f;
            XPBar.SetEXP(currentEXP);
            if (currentEXP > MaxEXP)
            {
                currentEXP = MaxEXP;
            }
        }

        
        
    }

    void TakeDamage (int damage)
    {
        currentHealth -= damage;    
        healthBar.SetHealth(currentHealth);

        //Debug.Log("damage");
    }

    void Sprint(int staminacost)
    {
        currentStamina -= staminacost;
        staminaBar.SetStamina(currentStamina);

        speed += 1f;
    }

    bool IsPlayerMoving()
    {
       return 
       Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) ||
       Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) ||
       Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) ||
       Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D); 
       //return Mathf.Abs(rb.velocity.x) > 0.01f || Mathf.Abs(rb.velocity.y) >0.01f;

    }

    public void ClearExpBar ( )
    {
        currentEXP = 0;
    }


}