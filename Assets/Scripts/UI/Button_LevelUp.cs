using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Button_LevelUp : MonoBehaviour
{
    public Button levelUpButton;
    public PlayerMovement playerMovement;

    void Start()
    {
        levelUpButton = GetComponent<Button>();

        playerMovement = playerMovement.GetComponent<PlayerMovement>();
        levelUpButton.gameObject.SetActive(true);
        levelUpButton.transform.position = new Vector3(-1340.4f, 45.91f, 0.00f);

    }

    void Update()
    {
        float EXP = playerMovement.currentEXP;
        ChangeColor(EXP);
        Debug.Log("EXP: " + EXP);
        Debug.Log("pozycja " + levelUpButton.transform.position);
        
    }

    void ChangeColor(float EXP)
    {

        if (EXP < 10)
        {
            levelUpButton.transform.position = new Vector3(-1340.4f, 45.91f, 0.00f);
            Debug.Log("NIE LEVEL UP");
        } else if (EXP == 10)
        {
            levelUpButton.transform.position = new Vector3( 63.66f, 45.91f, 0.00f);
            levelUpButton.image.color= Color.yellow;
            Debug.Log("LEVEL UP!!");

        }
    }

}
