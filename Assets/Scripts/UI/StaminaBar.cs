using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public Slider sliderST;

    public void SetMaxStamina(float stamina)
    {
        sliderST.maxValue = stamina;
        sliderST.value = stamina;
    }

    public void SetStamina(float stamina)
    {
        sliderST.value = stamina;
    }

}
