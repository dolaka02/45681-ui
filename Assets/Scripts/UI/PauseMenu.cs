using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject PauseMenu;
    public bool IsPaused;
    public Image image;
    public bool img_disp = false;
    void Start()
    {
        //image = GetComponent<Image>();
        PauseMenu.SetActive(false);
        
        image.gameObject.SetActive(false);

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            image.gameObject.SetActive(false);
            img_disp = false;
            if (IsPaused) 
            { 
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
            
        }


    }

    public void PauseGame()
    {
        if (IsPaused)
        {
            PauseMenu.SetActive(false);
            Time.timeScale = 1.0f;
            IsPaused = false;
        }
        else { 
            PauseMenu.SetActive(true);
            Time.timeScale = 0.0f;
            IsPaused = true;
        }
    }    
    public void ResumeGame()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1.0f;
        IsPaused = false;
    }

    public void MainMenu()
    {
        if (!img_disp) 
        { 
            image.gameObject.SetActive(true);
            img_disp = true;
        }
        else
        {
            image.gameObject.SetActive(false);
            img_disp = false;
        }
    }
}
