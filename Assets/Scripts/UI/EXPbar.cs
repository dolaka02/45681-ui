using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


 
public class EXPbar : MonoBehaviour
{
    public Slider sliderEXP;
    
    public void SetMinEXP(float EXP)
    {
        sliderEXP.minValue = EXP;
        sliderEXP.value = EXP;
    }

    public void SetEXP(float EXP)
    {
        sliderEXP.value = EXP;
}
    }


